library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stoppuhr is 

    port (
        reset_n     : in std_logic; -- Key 0
        clk         : in std_logic; --50 MHz
        cnt_enable  : in std_logic; -- SW 0: Start / Stop
        
        led_hex_a   : out std_logic_vector(6 downto 0); -- s-:--
        led_hex_b   : out std_logic_vector(6 downto 0); -- -s:--
        led_hex_c   : out std_logic_vector(6 downto 0); -- --:ms-
        led_hex_d   : out std_logic_vector(6 downto 0)  -- --:-ms
        
        );
end entity stoppuhr;

architecture arch of stoppuhr is

signal counter_10ms  : unsigned(3 downto 0);
signal counter_100ms : unsigned(3 downto 0);
signal counter_1s    : unsigned(3 downto 0);
signal counter_10s   : unsigned(3 downto 0);

signal overflow_10ms : unsigned(20 downto 0);

begin

    P:process(clk, reset_n)
    begin
        if (rising_edge(clk)) then

            --check if counter enabled
            if (cnt_enable = '1') then 
                overflow_10ms <= overflow_10ms + 1;
            end if;

            --7A120(16) = 500000(10), count each 10ms
            if (overflow_10ms >= X"7A120") then 
                counter_10ms <= counter_10ms + 1;
                overflow_10ms <= (others => '0');
            end if;

            --A(16) = 10(10), count each 100ms
            if (counter_10ms = X"A") then 
                counter_100ms <= counter_100ms + 1;
                counter_10ms <= (others => '0');
            end if;

            --A(16) = 10(10), count each 1s
            if (counter_100ms = X"A") then 
                counter_1s <= counter_1s + 1;
                counter_100ms <= (others => '0');
            end if;

            --A(16) = 10(10), count each 10s
            if (counter_1s = X"A") then 
                counter_10s <= counter_10s + 1;
                counter_1s <= (others => '0');
            end if;            

            if (counter_10s = X"6") then 
                counter_10s <= (others => '0');
            end if;

        end if;

        --reset if pressed
        if (reset_n = '0') then 
            counter_10ms <= (others => '0');
            counter_100ms <= (others => '0');
            counter_1s <= (others => '0');
            counter_10s <= (others => '0');
        end if;
    end process P;

    i_hex7_a : entity work.hex_to_seven(arch) port map(std_logic_vector(counter_10ms),  led_hex_a);
    i_hex7_b : entity work.hex_to_seven(arch) port map(std_logic_vector(counter_100ms), led_hex_b);
    i_hex7_c : entity work.hex_to_seven(arch) port map(std_logic_vector(counter_1s),  led_hex_c);
    i_hex7_d : entity work.hex_to_seven(arch) port map(std_logic_vector(counter_10s), led_hex_d);
        

end architecture arch;
    
    
    
    
    
    
    
    