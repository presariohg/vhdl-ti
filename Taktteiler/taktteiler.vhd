library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity taktteiler is 
    port (clk         : in std_logic; --50 MHz
          clk_10Hz    : out std_logic);

end entity taktteiler;

architecture arch of taktteiler is

signal counter : unsigned(22 downto 0);
signal clk_10Hz_int : std_logic;
begin
    P:process(clk)
    begin
        if (falling_edge(clk)) then
            counter <= counter + 1;

            if (counter > X"2625A0") then
				--2625A0(16) = 2500000(10) = 1/2 period
                clk_10Hz_int <= not clk_10Hz_int;
                counter <= (others => '0');
            end if;
        end if;
    end process P;


    clk_10Hz <= clk_10Hz_int;

end architecture arch;
    
    
    
    
    
    
    

