library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is 
    port (
        reset_n     : in std_logic; -- Key 3
        clk         : in std_logic; --50 MHz
        switches    : in std_logic_vector(7 downto 0); -- zur Übernahme des overflow-values
        cnt_enable  : in std_logic; -- SW9
        ofl_rd     : in std_logic; -- read and store ofl-value, KEY0
        cnt_rd      : in std_logic; -- read and store the actual count-value, KEY1
        cnt_val_act : out std_logic_vector(7 downto 0); -- aktueller Zählwert
        cnt_val_stored_out : out std_logic_vector(7 downto 0) -- gespeicherter Zählwert, LED      
        );
end entity counter;

architecture arch of counter is
    signal counter_10Hz_int : unsigned(7 downto 0);
    signal clk_10Hz         : std_logic;
    signal counter_50MHz_int: unsigned(22 downto 0);
    signal overflow_10Hz : unsigned(7 downto 0);
	 signal saved_counter : unsigned(7 downto 0);
begin
    P:process(clk, reset_n, ofl_rd)
    begin
        if (falling_edge(clk)) then

            counter_50MHz_int <= counter_50MHz_int + 1;

            --tick the 10Hz clock
            if (counter_50MHz_int = X"2625A0") then
                --2625A0(16) = 2500000(10) = 1/2 cycle
                counter_50MHz_int <= (others => '0');

                --check if counter enabled
                if (cnt_enable = '1') then
                    clk_10Hz <= not clk_10Hz;
                end if;

                --count at falling edge
                if (clk_10Hz = '0' and cnt_enable = '1') then
                    counter_10Hz_int <= counter_10Hz_int + 1;
                end if;

                if (counter_10Hz_int >= overflow_10Hz) then
                    counter_10Hz_int <= (others => '0');
                end if;
            end if;
				
				
				if (cnt_rd = '0') then
					saved_counter <= counter_10Hz_int;
				end if;
		  end if;

        --reset counter value if pressed
        if (reset_n = '0') then
            counter_10Hz_int <= (others => '0');
        end if;

        --change overflow value if pressed
        if (ofl_rd = '0') then
            overflow_10Hz <= unsigned(switches);
        end if;


    end process P;

    cnt_val_act <= std_logic_vector(counter_10Hz_int);
	 cnt_val_stored_out <= std_logic_vector(saved_counter);

end architecture arch;
