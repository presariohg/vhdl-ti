library ieee;
use ieee.std_logic_1164.all;

entity demux is 
    port (
        switch : in std_logic_vector(2 downto 0);
        led    : out std_logic_vector(7 downto 0);
        button : in std_logic
    );
end demux;

architecture demux_arc of demux is
begin
   
    led(0) <= not button when switch = "000";
    led(1) <= not button when switch = "001";
    led(2) <= not button when switch = "010";
    led(3) <= not button when switch = "011";
    led(4) <= not button when switch = "100";
    led(5) <= not button when switch = "101";
    led(6) <= not button when switch = "110";
    led(7) <= not button when switch = "111";
end demux_arc;
